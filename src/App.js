import React, { Component } from "react";
// import "./App.css";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import PostForm from "./component/PostForm";
import PostList from "./component/PostList";
import UpdatePost from "./component/UpdatePost";

class App extends Component {
  render() {
    return (
      <Router>
        <nav className="nav nav-dark bg-dark p-4 justify-content-center">
          <Link to="/post-list">
            <div className="p-3 text-light">Post List / Get API</div>
          </Link>
          <Link to="/update-post">
            <div className="p-3 text-light">Update List / Put API</div>
          </Link>
          <Link to="/create-post">
            <div className="p-3 text-light">Create List / Post API</div>
          </Link>
        </nav>
        <div className="container">
          <Switch>
            <Route path="/post-list">
              <PostList />
            </Route>
            <Route path="/update-post">
              <UpdatePost />
            </Route>
            <Route path="/create-post">
              <PostForm />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
// import React, { useEffect, useState } from "react";
// import "./App.css";
// import List from "./component/list";
// import withListLoading from "./component/withListLoading";

// function App() {
//   const ListLoading = withListLoading(List);
//   const [appState, setAppState] = useState({
//     loading: false,
//     repos: null,
//   });

//   useEffect(() => {
//     setAppState({ loading: true });
//     const apiUrl = `https://api.github.com/users/hacktivist123/repos`;
//     fetch(apiUrl)
//       .then((res) => res.json())
//       .then((repos) => {
//         setAppState({ loading: false, repos: repos });
//         console.log("Response of Api", repos);
//       });
//   }, [setAppState]);
//   return (
//     <div className="App">
//       <div className="container">
//         <h1>My Repositories</h1>
//       </div>
//       <div className="repo-container">
//         <ListLoading repos={appState.repos} />
//       </div>
//       <footer>
//         <div className="footer">
//           Built{" "}
//           <span role="img" aria-label="love">
//             💚
//           </span>{" "}
//           with by Ajmal
//         </div>
//       </footer>
//     </div>
//   );
// }
// export default App;
