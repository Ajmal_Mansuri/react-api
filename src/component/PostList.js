import axios from "axios";
import React, { Component } from "react";

class PostList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      userId: "",
      title: "",
      body: "",
      // id:[]
    };
  }

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((response) => {
        // console.log(response.data);
        this.setState({ posts: response.data });
        // console.log("In order to retrive id", id);
        // console.log(this.state.posts);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onClickStoreId(post) {
    const id = post.id;
    console.log("Post Selected : ", id);
    axios
      .delete("https://jsonplaceholder.typicode.com/posts/" + id)
      .then((response) => {
        console.log(response);
      });
    // debugger;
    // console.log(this.props.id);
  }

  render() {
    const { posts } = this.state;
    return (
      <div className="my-4">
        <h1 className="text-center"> List Of Post</h1>
        {posts.length ? (
          posts.map((post) => (
            <div key={post.id}>
              <div className="row my-3">
                <div className="col-2 text-center">{post.id}</div>
                <div className="col-6">{post.title}</div>
                <div className="col-2">
                  <button
                    className="btn-sm btn-primary float-right"
                    onClick={this.onClickStoreId.bind(this, post)}
                    style={{ cursor: "pointer" }}
                  >
                    Delete
                  </button>
                </div>
                <div className="col-2">
                  <button
                    className="btn-sm btn-primary"
                    style={{ cursor: "pointer" }}
                  >
                    EDIT
                  </button>
                </div>
              </div>
              {/* {post.id} */}
              {/* {post.title} */}
            </div>
          ))
        ) : (
          <div className="container"> No Ports Found </div>
        )}
      </div>
    );
  }
}

export default PostList;
