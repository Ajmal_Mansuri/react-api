

import React, { Component } from "react";
import axios from "axios";

class PostForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userId: "",
      title: "",
      body: "",
    };
  }

  submitHandler = (e) => {
    e.preventDefault();
    console.log(this.state);
    axios
      .post("https://jsonplaceholder.typicode.com/posts", this.state)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    //   [e.target.name]: e.target.value
  };
  render() {
    const { userId, title, body } = this.state;
    return (
      <div>
        <form className="container mt-5" onSubmit={this.submitHandler}>
          <div className="row">
          <label className="col-2">UserId</label>
            <div className="col-8" >
              <div className="form-group">
                <input
                  type="number"
                  name="userId"
                  value={userId}
                  onChange={this.changeHandler}
                  style={{ width: "-webkit-fill-available" }}
                />
              </div>
            </div>
          </div>
          <div className="row ">
          <label className="col-2">Title</label>
            <div className="col-8"  >
              <div className="form-group">
                <input
                type="text"
                name="title"
                value={title}
                onChange={this.changeHandler}
                style={{ width: "-webkit-fill-available" }}
                />
              </div>
            </div>
          </div>
          <div className="row ">
          <label className="col-2">Body</label>
            <div className="col-8" >
              <div className="form-group">
                <input
                 type="text"
                 name="body"
                 value={body}
                 onChange={this.changeHandler}
                 style={{ width: "-webkit-fill-available" }}
                />
              </div>
            </div>
          </div>
          <button className="btn-primary " type="submit">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default PostForm;
