import axios from "axios";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import React, { Component } from "react";

class UpdatePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      title: "",
      body: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit = (event) => {
    // alert('An essay was submitted: ' + this.state.value);
    console.log(this.state);
    const userId = this.state.id;
    axios
      .put("https://jsonplaceholder.typicode.com/posts/" + userId, this.state)

      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
    event.preventDefault();
  };

  handleChange = (event) => {
    this.setState({ id: event.target.id });
    this.setState({ title: event.target.title });
    this.setState({ body: event.target.body });
    // this.setState
  };

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/posts/7")
      .then((response) => {
        this.setState({
          id: response.data.id,
          title: response.data.title,
          body: response.data.body,
        });
        console.log("Data Stored in Post Function------", this.state.id);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit} className="container mt-5">
        <div className="row">
          <label className="col-2">Id</label>
          <div className="col-8">
            <div className="form-group">
              <InputText
                name="id"
                type="text"
                value={this.state.id}
                style={{ width: "-webkit-fill-available" }}
                // checked={this.state.id}
                onChange={this.handleChange}
              />
            </div>
          </div>
        </div>
        <br />
        <div className="row">
                    <label className="col-2">Title</label>
                    <div className="col-8" >
                        <div className="form-group">
                            <InputTextarea
                                rows={2}
                                cols={20}
                                name="title"
                                type="text"
                                value={this.state.title}
                                onChange={this.handleChange}
                                style={{ width: "-webkit-fill-available" }}
                            />
                        </div>
                    </div>
                </div>
        <div className="row">
          <label className="col-2">Body</label>
          <div className="col-8">
            <div className="form-group">
              <input
                name="body"
                type="text"
                value={this.state.body}
                onChange={this.handleChange}
                style={{ width: "-webkit-fill-available" }}
              />
            </div>
          </div>
        </div>
        <br />
        <input type="submit" value="Submit" className="btn-primary" />
      </form>
    );
  }
}

export default UpdatePost;
